#!/bin/sh

WIFI=`networksetup -listallhardwareports | awk '$3=="Wi-Fi" {getline; print $2}'`

MAC=`ifconfig $WIFI | grep ether | cut -d ' ' -f 2`
echo "Old MAC address for $WIFI is: $MAC."

NEW_MAC=`openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'`
echo "New MAC address for $WIFI will be: $NEW_MAC. Enter your password to confirm."

sudo -k ifconfig $WIFI ether $NEW_MAC

if [ $? -ne 0 ]; then
  exit 1
fi

MAC=`ifconfig $WIFI | grep ether | cut -d ' ' -f 2`
echo "Done. Updated MAC address for $WIFI is: $MAC."

echo "Turning off wi-fi on $WIFI"
networksetup -setairportpower $WIFI off

for i in {1..3}; do
  printf "$i..."
  sleep 1
done

echo ""
echo "Turning on wi-fi on $WIFI"
networksetup -setairportpower $WIFI on

