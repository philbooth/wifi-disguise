# wifi-disguise

*In which our hero triumphs over his dastardly nemesis.*

[![Package status](https://img.shields.io/npm/v/wifi-disguise.svg?style=flat-square)](https://www.npmjs.com/package/wifi-disguise)
[![License](https://img.shields.io/github/license/philbooth/wifi-disguise.svg?style=flat-square)](https://opensource.org/licenses/MIT)

* [What is it?](#what-is-it)
* [But why?](#but-why)
* [What doesn't it do?](#what-doesnt-it-do)
* [How do I install it?](#how-do-i-install-it)
* [How do I use it?](#how-do-i-use-it)
* [What license is it released under?](#what-license-is-it-released-under)

## What is it?

A script
that automates
changing the MAC address
of your wireless network adaptor
on OS X.

## But why?

To confound those
infuriating little twerps
who seek to prevent me
from getting work done
while using their bus network.

*I paid for that sodding ticket!*

## How do I install it?

```
npm i -g wifi-disguise
```

## How do I use it?

Just run the command `disguise`,
with no arguments:

```
disguise
```

The script will tell you
what it's doing as it runs.
You will need to
enter your password
to authorize
updating your MAC address.
If you change your mind,
you can cancel the script
with `^C`.

Example output:

```
Old MAC address for en0 is: 70:3e:53:4c:90:73.
New MAC address for en0 will be: 24:b0:14:0a:4f:2b. Enter your password to confirm.
Password:
Done. Updated MAC address for en0 is: 24:b0:14:0a:4f:2b.
Turning off wi-fi on en0
1...2...3...
Turning on wi-fi on en0
```

## What license is it released under?

[MIT](LICENSE).

