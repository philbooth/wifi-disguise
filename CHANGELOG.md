# Change log

## 1.0.2

* fix: update example script output (37f8e1d)
* chore: tabs => spaces (f0f76bd)
* fix: remove hardcoded en0 in ifconfig command (09077c2)

## 1.0.1

* fix: tweak the script output for accuracy (ebb8b4a)

## 1.0.0

* feat: implement a simple mac address changer for osx

